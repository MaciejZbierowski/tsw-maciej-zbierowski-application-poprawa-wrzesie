/* jshint node: true */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var questionSchema = new Schema({
    question : String,
    answer : String,
    category : String,
    answered : Boolean
});

module.exports = mongoose.model("Question", questionSchema);