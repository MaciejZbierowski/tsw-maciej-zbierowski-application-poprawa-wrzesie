/* jshint node: true, jquery: true, browser: true, devel: true */
/* global io: false */

$(document).ready(function(){
    
    var socket;
    
    if(!socket || !socket.connected)
        socket = io({forceNew: true});
    
    var $users = $('#users ul');
    var $sendgamename = $('#chooseGame #leftPart form input[type=submit]');
    var $gamename = $('#chooseGame #leftPart form input[type=text]');
    var $gameinfo = $('#chooseGame #leftPart p');
    var $games = $('#chooseGame #rightPart ul');
    var $creategameform = $('#chooseGame #leftPart form');
    var $collecting = $('#collectingplayers');
    var $gameboard = $('#game');
    
    $('form').submit(function(event){
        event.preventDefault();
    });
    
    //$collecting.hide();
    $('#chooseGame p#left').hide();
    $gameboard.hide();
    $('#auction').hide();
    $('#answer').hide();
    $('#question').hide();
    $('#categories').hide();
    $('#gameli').hide();
    $('#winner').hide();
    
    socket.on('players', function(users){
        $users.empty();
        $.each(users, function(name){
            $users.append('<li>' + users[name].name + '</li>');
        });
    });
    
    $sendgamename.on('click', function(){
        if($gamename.val() !== ''){
            socket.emit('create game', $gamename.val());
            socket.on('unique game', function(x){
                if(x){
                    $creategameform.hide();
                    $('#leftPart label').hide();
                }
                else{
                    socket.on('created game', function(data){
                        $gameinfo.text(data);
                    });
                }
            });
        }
        $gamename.val('');
        $gameinfo.text('');
        $('#collectingplayers p#nomore').text('');
        $('#winner').hide();
    });
    
    $sendgamename.keypress(function(e){
        
        if(e.which == 13){
            if($gamename.val() !== ''){
                socket.emit('create game', $gamename.val());
                socket.on('unique game', function(x){
                    if(x){
                        $('#leftPart').hide();
                    }
                    else{
                        socket.on('created game', function(data){
                            $gameinfo.text(data);
                        });
                    }
                });
            }
            $gamename.val('');
            $gameinfo.text('');
            $('#collectingplayers p#nomore').text('');
            $('#winner').hide();
        }
        
    });
    
    socket.on('games', function(games){
        if(Object.keys(games).length === 0){
            $games.empty();
            $('#rightPart label').text('Narazie nie ma żadnych gier');
        }
        else{
            $('#rightPart label').text('');
            $games.empty();
            $.each(games, function(name){
                $games.append('<li>' + games[name].name + '</li>');
            });
        }
    });
    
    $games.delegate('li', 'click', function(){
        
        var clickedLi = $(this).text();
        var gamename = $('#gameli label span').text();
        
        if(gamename !== clickedLi || gamename !== '' || clickedLi !== ''){
            socket.emit('join game', clickedLi);
            $collecting.show();
            $('#rightPart').hide();
            $('#gameli').show();
            socket.on('game name', function(name){
                $('#user label span').text(name);
            });
            socket.on('people in game', function(amount){
                if(amount === 3){
                    $gameboard.show();
                    socket.emit('start game');
                    $('#collectingplayers p#nomore').text('Gra już się rozpoczęła. Wybierz inną');
                }
                $('#collectingplayers p#waiting').text('Waiting for ' + (3 - amount) + ' players');
            });
        }
        else{
            $('#collectingplayers').show();
            $('#collectingplayers p#waiting').text('');
            $('#collectingplayers p#nomore').text('Juz byles w tej grze. Wybierz inną');
        }
        
    });
    
    socket.on('send question number', function(number){
        $('#numbers').find('.getOrange').removeClass("getOrange");
        $('#numbers div').addClass("getWhite");
        $('#numbers div:contains(' + number + ')').removeClass("getWhite");
        $('#numbers div:contains(' + number + ')').addClass("getOrange");
    });
    
    socket.on('send category', function(cat){
        $('#categories p span').text(cat);
    });
    
    socket.on('show auction field', function(){
        $('#collectingplayers').hide();
        $('#auction').show();
        $('#leftPart').hide();
    });
    
    socket.on('send pool', function(pool){
        $('#pool p span').text(pool);
    });
    
    socket.on('send players', function(players){
        $('#auction ul').empty();
        $.each(players, function(i){
            //////
            if(players[i].ingame !== null){
                $('#auction ul').append('<div><p class="field">Nazwa gracza</p><p class="name">'+ players[i].name + '</p><p class="field">Stan konta</p><p class="points"> ' + players[i].points + '</p><p class="field">Propozycja</p><p class="propose">' + players[i].propose + '</p></div>');
            }
            
        });
        
    });
    
    $('#auction form input[type=submit]').on('click', function(){
        $('#answerinfo p').text('');
        var user = $('#user p span').html();
        var proposetosend = $('#game #auction ul div:contains(' + user + ') p.propose').html();
        var propose = $('#auction form input[type=text]');
        var playerpoints = $('#game #auction ul div:contains(' + user + ') p.points').html();
        var all = parseInt(proposetosend) + parseInt(playerpoints);
        var poolnew = $('#pool p span').text();
        
        if(propose.val() !== '' && propose.val() > parseInt(proposetosend) && propose.val() < parseInt(playerpoints) && parseInt(propose.val()) % 100 === 0){
            socket.emit('send propose', propose.val(), all, poolnew);
        }else if(propose.val() === ''){
            $('#answerinfo p').text('Musisz wpisać wartość');
        }else if(propose.val() <= parseInt(proposetosend)){
            $('#answerinfo p').text('Wartość musi być większa od już proponowanej');
        }else if(propose.val() >= parseInt(playerpoints)){
            $('#answerinfo p').text('Wartość musi być mniejsza niż twoja suma punktów. Jeśli chcesz dac wszystkie swoje punkty, kliknij Vabank');
        }else if(parseInt(propose.val()) % 100 !== 0){
            $('#answerinfo p').text('Wartość musi być wielokrotnością 100');
        }
        
        propose.val('');
        
    });
    
    $('#auction button#vabank').on('click', function(){
        var pool = $('#pool p span').text();
        socket.emit('vabank', pool);
        $('#answerinfo p').text('');
    });
    
    $('#user button').on('click', function(){
        var gamename = $('#user label span').text();
        
        if(window.confirm("Are you suree?")){
            socket.emit('leave game', gamename);
        }
        
    });
    
    socket.on('remove player', function(name){
        $('#auction ul div:contains(' + name + ')').css('display', 'none');
    });
    $('#chooseGame p#left').hide();
    socket.on('remove operations', function(gamename){
        $('#game').hide();
        $('#leftPart, #rightPart').show();
        $('#chooseGame p#left').show();
        $('#chooseGame p#left span').text(gamename);
    });
    
    $('#auction button#stop').on('click', function(){
        var cat = $('#categories p span').text();
        socket.emit('stop auction', cat);
    });
    
    socket.on('stop actions', function(){
        $('#auction form').hide();
    });
    
    socket.on('answer show', function(){
        $('#answer').show();
    });
    
    socket.on('left socket operations', function(){
        $('#auction form').hide();
        $('#answer').show();
        $('#question').show();
        $('#categories').show();
    });
    
    socket.on('send question', function(question){
        $('#question p span').text(question);
    });
    
    $('#answer input[type=submit]').on('click', function(){
        var ans = $('#answer input[type=text]');
        var qs = $('#question p span').text();
        var pool = $('#pool p span').text();
        var qnr = $('#numbers').find('.getOrange').html();
        var gamename = $('#user label span').text();
        
        if(ans.val() !== '')
            socket.emit('send answer', ans.val(), qs, pool, qnr, gamename);
        
        ans.val('');
    });
    
    socket.on('answer info', function(info){
        $('#answerinfo p').text(info);
    });
    
    socket.on('answer actions', function(){
        $('#question').hide();
        $('#answer').hide();
        $('#auction form').show();
        $('#categories').hide();
    });
    
    socket.on('bad answer actions', function(info){
        $('#categories, #auction form, #answer, #question').hide();
        $('#winner').show();
        $('#winner p').text(info);
        $('#answerinfo p').text('');
    });
    
    socket.on('winner actions', function(winner){
        $('#categories, #question, #answer').hide();
        $('#answerinfo p').text('Wygrałeś');
        $('#winner').show();
    });
    
    $('#winner button').on('click', function(){
        var user = $('#user p span').html();
        var game = $('#gameli label span').text();
        socket.emit('play again', user, game);
    });
    
    socket.on('play again actions', function(gamename){
        $('#game, #winner').hide();
        $('#chooseGame, #leftPart, #rightPart, #chooseGame p#left, #leftPart label, #leftPart form').show();
        $('#chooseGame p#left span').text(gamename);
        //change
        $('#gameli').hide();
    });
    
    socket.on('hide info', function(){
        $('#chooseGame p#left span').text('');
        $('#chooseGame p#left').hide();
    });
    
});