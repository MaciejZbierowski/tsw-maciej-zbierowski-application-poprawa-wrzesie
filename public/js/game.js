/* jshint node: true */

function Game (name, id, owner){
    this.name = name;
    this.id = id;
    this.owner = owner;
    this.people = [];
    this.status = 'available';
}

Game.prototype.addPerson = function(personID){
    
    if(this.status === 'available'){
        this.people.push(personID);
    }
    
};

Game.prototype.removePerson = function(personID){
    this.people.splice(this.people.indexOf(personID), 1);
};

Game.prototype.findPerson = function(personID){
    
    var check = true;
    
    for(var i = 0; i < this.people.length; i++)
        if(this.people[i] === personID)
            check = false;
    
    return check;
    
};

module.exports = Game;