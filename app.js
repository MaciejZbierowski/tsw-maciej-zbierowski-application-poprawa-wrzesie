/* jshint node: true, jquery: true */

var express = require('express');
var expressValidator = require('express-validator');
var app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var mongoose = require('mongoose');
var httpServer = require('http').Server(app);
var static = require('serve-static');

var io = require('socket.io')(httpServer);
var pasportSocketIO = require('passport.socketio');
var MongoStore = require('connect-mongo')(expressSession);

var passport = require('passport');
var passportLocal = require('passport-local');
var passportHttp = require('passport-http');

var port = process.env.PORT || 3000;
var secret = process.env.APP_SECRET || 'secret';
var configDB = require('./config/database');

var uuid = require('node-uuid');
var Game = require('./public/js/game.js');

var User = require('./models/user');
var Question = require('./models/question');
mongoose.connect(configDB.url);
var db = mongoose.connection;

db.on('open', function(){
    console.log('Connected to MongoDB');
});
db.on('error', console.error.bind(console, 'MongoDB Error'));

var oneDay = 86400000;

app.use('/img', static(__dirname + '/public/img', { maxAge: oneDay }));
app.set('view engine', 'ejs');
app.use('/js/jquery.min.js', static(__dirname + '/bower_components/jquery/dist/jquery.min.js'));
app.use(static(__dirname + '/public'));

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(expressValidator());

app.use(expressSession({
    key: 'express.sid',
    secret: secret,
    store: new MongoStore({
        host: 'ds041992.mongolab.com',
        port: '41992',
        db: 'appdb',
        username: 'mongouser',
        password: 'mongopassword'
    }),
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());

io.use(pasportSocketIO.authorize({
    cookieParser: cookieParser,
    key: 'express.sid',
    secret: secret,
    store: new MongoStore({
        host: 'ds041992.mongolab.com',
        port: '41992',
        db: 'appdb',
        username: 'mongouser',
        password: 'mongopassword'
    }),
    success: onAuthorizeSuccess,
    fail: onAuthorizeFail
}));

function onAuthorizeSuccess(data, accept){
    //console.log('successful connection to socket.io');
    accept();
}

function onAuthorizeFail(data, message, error, accept){
    if(error)
        throw new Error(message);
    //console.log('failed connection to socket.io');
    
    if(error)
        accept(new Error(message));
}

var newusers;

var validateUser = function(username, password, done){
    /*User.find(function(err, q){
        if(err) return console.error(err);
            newusers = q;
    });*/
    
    User.findOne({username: username}, function(err, user){
        
        if(err){
            done(err);
        }
        if(user){
            if(user.password === password){
                done(null, user);
            } else{
                done(null, null,{message: 'Wrong username or password'});
            }
        } else{
            done(null, null);
        }
    });
};

passport.use(new passportLocal.Strategy(validateUser));
passport.use(new passportHttp.BasicStrategy(validateUser));

passport.serializeUser(function(user, done){
    done(null, user.id);
});
passport.deserializeUser(function(id, done){
    User.findOne({"_id": id}, function(err, user){
        if(err){
            done(err);
        }
        if(user){
            done(null, {
                id: user._id,
                name: user.name,
                username: user.username,
                password: user.password
            });
        } else {
            done({
                msg: 'Nieznany ID'
            });
        }
    });
});

var questions;
Question.find(function(err, q){
    if(err) return console.error(err);
    questions = q;
});

app.get('/', function(req, res){
    res.render('index', {
        isAuthenticated: req.isAuthenticated(),
        user: req.user
    });
});

app.get('/login', function(req, res){
    var errors = [];
    res.render('login', {message: req.session.messages});
});
/*
app.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
}), function(req, res){
    
    req.checkBody('username', 'Nazwa użytkownika nie może być pusta').notEmpty();
    
    var errors = req.validationErrors();
    if (errors) {
        res.send('There have been validation errors.', 400);
        return;
      }
      res.json({
        urlparam: req.params.urlparam,
        getparam: req.params.getparam,
        postparam: req.params.postparam
      });
    
    res.redirect('/');
  

    
});*/

app.post('/login',function(req,res){

    console.log('username == ' + req.body.username);
    
    
        console.dir(players);
    
    var check = true;
    
    if(players.length > 0){
        //check = true;
        for(var p = 0; p < players.length; p++){
            console.log('players name == ' + players[p].name);
            if(players[p].name === req.body.username)
                check = false;
        }
    }
    
    console.log('check == ' + check);
    
    req.checkBody('username', 'Username is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();

    //validate 
    var errors = req.validationErrors();

    if (errors) {
        res.render('login',{message: req.session.messages});
    }
    //else if(!check){
    //    res.redirect('/');
    //}
    else{
        if(check){
                passport.authenticate('local',{
                successRedirect:'/',
                failureRedirect: '/login',
                failureFlash : true  
            })(req,res);
        }else{
            res.render('login',{message: req.session.messages});
        }
        
    }
});

app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});

var players = [];
var clients = [];
var games = {};
var POINTS = 5000;
var POOL = 0;

io.sockets.on('connection', function(socket){
 
    var findElement = function(sth, object){
        var element;
        
        for(var p in object)
            if(object[p].name === sth)
                element = p;
        return element;
    };
    
    var checkifexists = function(name, obj){
        
        var check = true;
        
        for (var x in obj)
            if(obj[x].name === name)
                check = false;
        
        return check;
    };
    
    if(checkifexists(socket.request.user.username, players)){
        var newPlayer = {
            "id": uuid.v4(),
            socket: socket.id,
            "name" : socket.request.user.username,
            "ingame" : null,
            "points": POINTS,
            "propose" :  200,
            "clicked": 0
        };
        players.push(newPlayer);
        clients.push(socket);
    }
    
    io.sockets.emit('players', players);
    io.sockets.emit('games', games);
    
    socket.on('create game', function(gamename){
        
        var player = findElement(socket.request.user.username, players);
        
        socket.emit('unique game', checkifexists(gamename, games));
        
        if(checkifexists(gamename, games)){
            var id = uuid.v4();
            var game = new Game(gamename, id, players[player].id);
            games[id] = game;
            io.sockets.emit('games', games);
        }
        else if(!checkifexists(gamename, games))
            socket.emit('created game', 'Już istnieje taka gra. Wybierz inną nazwę');
    });
    
    socket.on('join game', function(gamename){
        
        var player = players[findElement(socket.request.user.username, players)];
        var game = games[findElement(gamename, games)];
        socket.emit('game name', game.name);
        socket.emit('game games', player.games);
        
        game.addPerson(player.id);
        player.ingame = gamename;
        socket.room = game.name;
        socket.join(socket.room);
        
        io.sockets.in(socket.room).emit('people in game', game.people.length);
    });
    
    socket.on('start game', function(){
        
        var player = players[findElement(socket.request.user.username, players)];
        POOL = 0;
        
        io.sockets.in(socket.room).emit('show auction field');
        io.sockets.in(socket.room).emit('send question number', 1);
        
        player.points -= player.propose;
        for (var p in players)
            POOL += POINTS - players[p].points;
        io.sockets.in(socket.room).emit('send pool', POOL);
        io.sockets.in(socket.room).emit('send players', players);
        
    });

    socket.on('send propose', function(propose, points, sendpool){
        
        var player = players[findElement(socket.request.user.username, players)];
        
        POOL += parseInt(propose) - player.propose;
        
        player.propose = propose;
        player.points -= parseInt(propose) - (parseInt(points) - player.points);
        io.sockets.in(socket.room).emit('send players', players);
        io.sockets.in(socket.room).emit('send pool', POOL);
        
    });
    
    socket.on('vabank', function(pool){
        
        var player = players[findElement(socket.request.user.username, players)];
        var x = parseInt(pool);
        
        player.propose = player.points;

        x += player.points;
        
        player.points = 0;
        
        io.sockets.in(socket.room).emit('send pool', x);
        io.sockets.in(socket.room).emit('stop actions');
        socket.emit('left socket operations');
        io.sockets.in(socket.room).emit('send players', players);
        socket.broadcast.in(socket.room).emit('answer info', player.name + ' odpowiada');
        
        var qn;
                
        do{
            qn = Math.floor((Math.random() * questions.length) + 0);
        }while(!((qn >= 0 && qn <= questions.length) && questions[qn].answered === false));
        questions[qn].answered = true;
        socket.emit('send question', questions[qn].question);
        socket.emit('send category', questions[qn].category);
    });
    
    socket.on('stop auction', function(category){
        
        var player = players[findElement(socket.request.user.username, players)];
        
        var times = 0;
        
        player.clicked = 1;
        
        for(var p in players)
            if(players[p].clicked === 1)
                times++;
        
        if(times <= players.length-1){
            socket.emit('stop actions');
            if(times === players.length-1){
                var leftuser;
                var leftsocket;
                
                for(p in players)
                    if(players[p].clicked === 0)
                        leftuser = players[p];

                for(var t = 0; t < clients.length; t++)
                    if(clients[t].id === leftuser.socket)
                        leftsocket = clients[t];
                
                var qn;
                
                do{
                    qn = Math.floor((Math.random() * questions.length) + 0);
                }while(!((qn >= 0 && qn <= questions.length) && questions[qn].answered === false));
                questions[qn].answered = true;
                
                leftsocket.emit('left socket operations');
                leftsocket.broadcast.in(socket.room).emit('answer info', leftuser.name + ' odpowiada na pytanie');
                leftsocket.emit('send question', questions[qn].question);
                io.sockets.in(socket.room).emit('send category', questions[qn].category);
            }
        }
    });
    
    socket.on('send answer', function(answer, question, sendpool, questionnumber, gamename){
        
        var foundanswer;
        var player = findElement(socket.request.user.username, players);
        var game = findElement(gamename, games);
        var senquestion = parseInt(questionnumber);
        
        for(var q in questions)
            if(questions[q].question === question)
                foundanswer = questions[q].answer;
        
        senquestion++;
        
            if(senquestion <= 6){
                if(answer === foundanswer){

                    socket.emit('answer info', 'Gratulacje, dobra odpowiedź! Pula zostaje dopisana do twojego stanu konta :)');
                    socket.broadcast.in(socket.room).emit('answer info', players[player].name + ' poprawnie odpowiedział na pytanie.');
                    players[player].points += parseInt(sendpool);

                    POOL = 0;

                    for (var p in players){
                        players[p].propose = 200;
                        players[p].points -= players[p].propose;
                        POOL += players[p].propose;
                        players[p].clicked = 0;
                    }

                    io.sockets.in(socket.room).emit('send pool', POOL);
                    io.sockets.in(socket.room).emit('send players', players);
                    io.sockets.in(socket.room).emit('answer actions');
                    io.sockets.in(socket.room).emit('send question number', senquestion);
                }
                else{
                    if(players[player].points === 0){
                        socket.broadcast.in(socket.room).emit('answer info', players[player].name + ' postawił wszystkie swoje punkty i źle odpowiedział na pytanie. Punkty z puli przechodzą do następnego pytania. ' + players[player].name + ' kończy grę.');
                        
                        games[game].removePerson(players[player].id);
                        players.splice(players.map(function(e) { return e.name; }).indexOf(socket.request.user.username), 1);
                        clients.splice(clients.map(function(e) { return e.id; }).indexOf(player.socket), 1);
                        
                        io.sockets.emit('players', players);
                        
                        if(players.length !== 1){
                            POOL = parseInt(sendpool);

                            for(var ppp in players){
                                players[ppp].propose = 200;
                                players[ppp].points -= players[ppp].propose;
                                POOL += players[ppp].propose;
                                players[ppp].clicked = 0;
                            }

                            io.sockets.in(socket.room).emit('send pool', POOL);
                            
                            socket.emit('bad answer actions', 'Postawiłeś wszystkie swoje punkty i niestety nie udało Ci się poprawnie odpowiedzieć. Punkty z puli przechodzą do następnego pytania. Ty kończysz grę');
                            io.sockets.in(socket.room).emit('send players', players);
                            socket.broadcast.in(socket.room).emit('answer actions');
                            io.sockets.in(socket.room).emit('send question number', senquestion);
                        }else{
                            
                            players[0].points += parseInt(sendpool);
                            players[0].propose = 0;
                            
                            io.sockets.in(socket.room).emit('send pool', 0);
                            io.sockets.in(socket.room).emit('send players', players);
                            
                            var ss;
                            
                            for(var i = 0; i < clients.length; i++)
                                if(clients[i].id === players[0].socket)
                                    ss = clients[i];
                            
                            ss.emit('bad answer actions', 'Reszta graczy zrezygnowała. Wygrałeś walkowerem');
                            ss.broadcast.in(socket.room).emit('bad answer actions', players[0].name + ' wygrał');
                        }
                        socket.leave(socket.room);
                    }
                    
                    else{
                        socket.emit('answer info', 'Niestety nie udało Ci się poprawnie odpowiedzieć. Punkty z puli przechodzą do następnego pytania');
                        socket.broadcast.in(socket.room).emit('answer info', players[player].name + ' źle odpowiedział na pytanie. Punkty z puli przechodzą do następnego pytania');

                        POOL = parseInt(sendpool);

                        for(var pp in players){
                            players[pp].propose = 200;
                            players[pp].points -= players[pp].propose;
                            POOL += players[pp].propose;
                            players[pp].clicked = 0;
                        }

                        io.sockets.in(socket.room).emit('send pool', POOL);
                        io.sockets.in(socket.room).emit('send players', players);
                        io.sockets.in(socket.room).emit('answer actions');
                        io.sockets.in(socket.room).emit('send question number', senquestion);
                    }
                }
            }
            else{

                var winner;
                
                players[player].points += parseInt(sendpool);
                delete games[game];
                io.sockets.emit('games', games);
                
                for(var w in players)
                    if(players[w].points === Math.max.apply(Math, players.map(function(o){return o.points;})))
                        winner = players[w].name;

                for(w in players){
                    players[w].ingame = null;
                    players[w].propose = 0;
                }
                
                io.sockets.in(socket.room).emit('winner actions', winner);
                io.sockets.in(socket.room).emit('send pool', 0);
                io.sockets.in(socket.room).emit('send players', players);
            }

    });
    
    socket.on('play again', function(playername, gamename){
        
        io.sockets.emit('games', games);
        if(checkifexists(socket.request.user.username, players)){
            var newPlayer = {
                "id": uuid.v4(),
                socket: socket.id,
                "name" : socket.request.user.username,
                "game" : null,
                "ingame" : null,
                "points": POINTS,
                "propose" :  200,
                "clicked": 0
            };
            players.push(newPlayer);
            clients.push(socket);
        }
        else{
            for(var p in players){
                players[p].ingame = null;
                players[p].points = POINTS;
                players[p].propose = 200;
                players[p].clicked = 0;
            }
        }
        io.sockets.emit('players', players);
        socket.emit('play again actions', gamename);
    });
    
    socket.on('leave game', function(gamename){
        
        var player = players[findElement(socket.request.user.username, players)];
        var game = findElement(gamename, games);
        
        games[game].removePerson(player.id);
        players.splice(players.map(function(e) { return e.name; }).indexOf(socket.request.user.username), 1);
        io.sockets.in(socket.room).emit('send players', players);
        io.sockets.emit('players', players);
        clients.splice(clients.map(function(e) { return e.id; }).indexOf(player.socket), 1);
        socket.emit('bad answer actions', '');
        socket.leave(socket.room);

        if(games[game].people.length === 1){
            io.sockets.emit('send players', players);
            io.sockets.in(socket.room).emit('bad answer actions', players[0].name + ' wygrał');
            io.sockets.emit('games', games);
            io.sockets.emit('hide info');
            io.sockets.emit('hide info');
            delete games[game];
            for(var i = 0; i < clients.length; i++)
                if(clients[i].id === players[i].socket)
                    clients[i].leave(socket.room);
        }
        
    });
    
    socket.on('disconnect', function(){
        
        var player = players[findElement(socket.request.user.username, players)];
        var game;
        
        if(player.ingame === null){
            players.splice(players.map(function(e) { return e.name; }).indexOf(socket.request.user.username), 1);
            io.sockets.emit('players', players);
            clients.splice(clients.map(function(e) { return e.id; }).indexOf(player.socket), 1);
        }
        else{
            
            for(var g in games)
                if(games[g].findPerson(player.id) === false)
                    game = g;
            
            games[game].removePerson(player.id);
            socket.leave(socket.room);
            players.splice(players.map(function(e) { return e.name; }).indexOf(socket.request.user.username), 1);
            io.sockets.emit('players', players);
            io.sockets.emit('send players', players);
            clients.splice(clients.map(function(e) { return e.id; }).indexOf(player.socket), 1);
            
            if(games[game].people.length === 1){
                clients[0].emit('bad answer actions', players[0].name + ' wygrał');
                delete games[game];
            }
            
        }
        
    });
    
});

httpServer.listen(port, function(){
    console.log('http://localhost:' + port);
});